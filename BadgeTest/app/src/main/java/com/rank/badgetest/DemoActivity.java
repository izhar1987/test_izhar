package com.rank.badgetest;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import viewbadger.BadgeView;

public class DemoActivity extends Activity implements View.OnClickListener {

    View target;
    BadgeView badge;
    int count=0;

    Button btnAddBadge,btnMinuseBadge;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        // *** default badge ***
        target = findViewById(R.id.default_target);
        target.setOnClickListener(this);
        badge = new BadgeView(this, target);

        btnAddBadge=(Button)findViewById(R.id.btnAddBadge);
        btnMinuseBadge=(Button)findViewById(R.id.btnMinuseBadge);
        btnMinuseBadge.setOnClickListener(this);
        btnAddBadge.setOnClickListener(this);



    }

    @Override
    protected void onResume() {
        super.onResume();
        // *** test frame layout ***
    }

    @Override
    public void onClick(View v) {


        if(v==btnAddBadge){
            count=count+1;

            badge.setText(String.valueOf(count));
            badge.show();
            btnMinuseBadge.setClickable(true);
        }
        if(v==btnMinuseBadge){
            count=count-1;
            if(count==0){
               badge.setVisibility(View.GONE);
                btnMinuseBadge.setClickable(false);
            }else{
                badge.setText(String.valueOf(count));
                badge.show();
            }

        }
    }
}